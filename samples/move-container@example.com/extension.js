/**
 * GNOME Shell Extension Sample
 * 
 * Move container in GNOME Shell stage.
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const {St, GObject, Meta} = imports.gi;
const Main = imports.ui.main;
const DND = imports.ui.dnd;

let container1;

const MyContainer1 = GObject.registerClass(
class MyContainer1 extends St.Bin {

  _init () {
  
    super._init({
      style : 'background-color : gold',
      reactive : true,
      can_focus : true,
      track_hover : true,
      width : 120,
      height : 120,
      x : 0,
      y : 0,
    });
    
    this._delegate = this;
    
    this._draggable = DND.makeDraggable(this);
    
    this._draggable.connect("drag-begin", () => {
      this._setDragMonitor(true);
    });
    
    this._draggable.connect("drag-end", () => {
      this._setDragMonitor(false);
    });
    
    this._draggable.connect("drag-cancelled", () => {
      this._setDragMonitor(false);
    });
    
    this.connect("destroy", () => {
      this._setDragMonitor(false);
    });
  
  }
  
  _setDragMonitor (add) {
  
    if (add) {
      this._dragMonitor = {
        dragDrop : this._onDragDrop.bind(this),
      };
      DND.addDragMonitor(this._dragMonitor);
    } else if (this._dragMonitor) {
      DND.removeDragMonitor(this._dragMonitor);
    }
  
  }

  _onDragDrop (dropEvent) {
  
    this._draggable._dragState = DND.DragState.INIT;
    global.display.set_cursor(Meta.Cursor.DEFAULT);
    this._draggable._dragComplete();

    return DND.DragDropResult.SUCCESS;
  }

});


function init() {
  container1 = new MyContainer1();
}

function enable() {

  Main.layoutManager.addChrome(container1, {
    affectsInputRegion : true,
    trackFullscreen : true,
  });
}

function disable() {
  Main.layoutManager.removeChrome(container1);
}
