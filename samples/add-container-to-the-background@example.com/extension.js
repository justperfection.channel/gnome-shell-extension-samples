/**
 * GNOME Shell Extension Sample
 * 
 * Add a simple container to the GNOME Shell Background.
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const St = imports.gi.St;
const Main = imports.ui.main;

let container;

function init() {
  container = new St.Bin({
      style : 'background-color : gold',
      reactive : true,
      can_focus : true,
      track_hover : true,
      width : 200,
      height : 200,
      x : 100,
      y : 100,
    });
}

function enable() {
  Main.layoutManager._backgroundGroup.add_child(container);
}

function disable() {
  Main.layoutManager._backgroundGroup.remove_child(container);
}
