/**
 * GNOME Shell Extension Sample
 * 
 * Read and Write Files With Gio.
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const {Gio} = imports.gi;
const Me = imports.misc.extensionUtils.getCurrentExtension();

function init() {}

function enable() {
  
  let filepath = Me.dir.get_path() + '/file.txt';
  let file = Gio.File.new_for_path(filepath);
  
  // getting file content
  let [loadSuccess, contents] = file.load_contents(null);
  if (loadSuccess) {
    log('This Is The File Content:');
    log(contents);
  }
  
  // saving the content to the file
  let newContent = "New File Content Has Been Generated on " + new Date();
  let [replaceSuccess, tag] = file.replace_contents(
    newContent, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
  if (replaceSuccess) {
    log('File Content Has Been Changed');
  }
}

function disable() {}
