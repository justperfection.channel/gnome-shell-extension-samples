/**
 * GNOME Shell Extension Sample
 * 
 * Open and Close apps from GNOME Shell Extension
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const Util = imports.misc.util;

function init() {}

function enable() {
  Util.trySpawnCommandLine('gedit');
}

function disable() {
  Util.trySpawnCommandLine('killall gedit');
}
