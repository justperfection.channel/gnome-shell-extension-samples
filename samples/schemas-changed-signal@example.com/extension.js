/**
 * GNOME Shell Extension Sample
 * 
 * Schemas Changed Signal
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const {Gio} = imports.gi;
const Me = imports.misc.extensionUtils.getCurrentExtension();

let settings;

function getSettings() {

  let GioSSS = Gio.SettingsSchemaSource;
  
  let schemaSource = GioSSS.new_from_directory(
    Me.dir.get_child("schemas").get_path(),
    GioSSS.get_default(),
    false
  );
  
  let schemaObj = schemaSource.lookup(
    'org.gnome.shell.extensions.schemas-changed-signal', true);
    
  if (!schemaObj) {
    throw new Error('cannot find schemas');
  }
  
  return new Gio.Settings({ settings_schema : schemaObj });
}

function init() {

  settings = getSettings();

  // use 'changed::my-integer' signal for only my-integer change
  // use 'changed' signal for any change
  settings.connect('changed', (s, key_name) => {
    let value = s.get_int(key_name);
    log(`${key_name} value has been changed to ${value}`);
  });
}

function enable() {
  settings.set_int('my-integer', settings.get_int('my-integer') + 1);
  settings.set_int('my-integer2', settings.get_int('my-integer2') + 2);
}

function disable() {}

