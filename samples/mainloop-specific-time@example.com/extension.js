/**
 * GNOME Shell Extension Sample
 * 
 * Create a loop that can be called on specific hour:minute:seconds
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const Mainloop = imports.mainloop;
const GLib = imports.gi.GLib;
let timeout;

function loopIt (hour, minute, seconds, func) {
  
  let now = GLib.DateTime.new_now_local();
  
  let timeToLoop = GLib.DateTime.new_local(
    now.get_year(),
    now.get_month(),
    now.get_day_of_month(),
    hour,
    minute,
    seconds);
    
  let difference = timeToLoop.difference(now);
  
  let timeoutSeconds = 0;
  if (difference > -1) {
    // the requested time is after now (or equal)
    timeoutSeconds = Math.floor( difference / 1000000 );
  } else {
    // the requested time is before now
    // so we add 86400 (a day in seconds) at the end
    timeoutSeconds = Math.floor( Math.abs(difference) / 1000000 ) + 86400;
  }
  
  if (timeout) {
    Mainloop.source_remove(timeout);
  }
  timeout = Mainloop.timeout_add_seconds(timeoutSeconds, func);
  
  log( "From: " + now.format("%Y-%m-%d %H:%M:%S") );
  log( "To: " + timeToLoop.format("%Y-%m-%d %H:%M:%S") );
  log("Next loop will hapen in " + timeoutSeconds + " seconds");
}

function init() {
}

function enable() {	
  // now you can use it like this (will call the function on 06:30:00 PM)
  loopIt(18, 30, 0, function(){ return true; });
}

function disable() {
  Mainloop.source_remove(timeout);
}
